var Camera = {
  getPicture: function (filename, success, failure, options) {
    options = options || {};
    var quality = options.quality || 100;
    var orientation = options.orientation || 180;
    var correctOrientation = options.correctOrientation || 180;
    cordova.exec(success, failure, "Camera", "takePicture", [filename, quality, orientation, correctOrientation]);
  }
}

module.exports = Camera;