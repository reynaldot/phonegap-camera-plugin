package com.reynaldotortoledo.plugins.camera;

import static com.reynaldotortoledo.plugins.camera.CameraActivity.ERROR_MESSAGE;
import static com.reynaldotortoledo.plugins.camera.CameraActivity.FILENAME;
import static com.reynaldotortoledo.plugins.camera.CameraActivity.IMAGE_URI;
import static com.reynaldotortoledo.plugins.camera.CameraActivity.ORIENTATION;
import static com.reynaldotortoledo.plugins.camera.CameraActivity.QUALITY;
import static com.reynaldotortoledo.plugins.camera.CameraActivity.CORRECT_ORIENTATION;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

public class Camera extends CordovaPlugin {
	
	private CallbackContext callbackContext;
	
	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
		if (action.equals("takePicture")) {
			this.callbackContext = callbackContext;
			Context context = cordova.getActivity().getApplicationContext();
			Intent intent = new Intent(context, CameraActivity.class);
			intent.putExtra(FILENAME, args.getString(0));
			intent.putExtra(QUALITY, args.getInt(1));
			intent.putExtra(ORIENTATION, args.getInt(2));
			intent.putExtra(CORRECT_ORIENTATION, args.getInt(3));
			cordova.startActivityForResult(this, intent, 0);
			return true;
		}
		return false;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (resultCode == Activity.RESULT_OK) {
			callbackContext.success(intent.getExtras().getString(IMAGE_URI));
		} else {
			String errorMessage = intent.getExtras().getString(ERROR_MESSAGE);
			if (errorMessage != null) {
				callbackContext.error(errorMessage);
			} else {
				callbackContext.error("Failed to take picture");
			}
		}
	}
}