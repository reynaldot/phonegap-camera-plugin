package com.reynaldotortoledo.plugins.camera;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

public class CameraPreview extends ViewGroup implements SurfaceHolder.Callback {

	private Camera camera;
	private SurfaceView surfaceView;
	private SurfaceHolder holder;
	private Size previewSize;
	private List<Size> supportedPreviewSizes;
	
	
	public CameraPreview(Context context, SurfaceView surfaceView) {
		super(context);
		this.surfaceView = surfaceView;
		addView(surfaceView);
		holder = this.surfaceView.getHolder();
		holder.addCallback(this);
	}
	
	public void setCamera(Camera camera) {
		this.camera = camera;
		if (camera != null) {
			supportedPreviewSizes = camera.getParameters().getSupportedPreviewSizes();
			requestLayout();
		}
	}
	
	@Override
	protected void onMeasure(int widthMeasure, int heightMeasure) {
		final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasure);
		final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasure);
		setMeasuredDimension(width, height);
		
		if (supportedPreviewSizes != null) {
			previewSize = getOptimalPreviewSize(supportedPreviewSizes, width, height);
		}
	}
	
	private Size getOptimalPreviewSize(List<Size> sizes, int w, int h) {
		final double ASPECT_TOLERANCE = 0.1;
		double targetRatio = (double) w / h;
		if (sizes == null) return null;
		
		Size optimalSize = null;
		double minDiff = Double.MAX_VALUE;
		
		int targetHeight = h;
		
		for (Size size : sizes) {
			double ratio = (double) size.width / size.height;
			if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
			if (Math.abs(size.height - targetHeight) < minDiff) {
				optimalSize = size;
				minDiff = Math.abs(size.height - targetHeight);
			}
		}
		
		if (optimalSize == null) {
			minDiff = Double.MAX_VALUE;
			for (Size size : sizes) {
				if (Math.abs(size.height - targetHeight) < minDiff) {
					optimalSize = size;
					minDiff = Math.abs(size.height - targetHeight);
				}
			}
		}
		return optimalSize;
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		if (changed && getChildCount() > 0) {
			final View child = getChildAt(0);
			
			final int width = r - l;
			final int height = b - t;
			
			int previewWidth = width;
			int previewHeight = height;
			
			if (previewSize != null) {
				previewWidth = previewSize.width;
				previewHeight = previewSize.height;
			}
			
			if (width * previewHeight > height * previewWidth) {
				final int scaledChildWidth = previewWidth * height / previewHeight;
				child.layout((width - scaledChildWidth) / 2, 0, (width + scaledChildWidth) / 2, height);
			} else {
				final int scaledChildHeight = previewHeight * width / previewWidth;
				child.layout(0, (height - scaledChildHeight) / 2, width, (height + scaledChildHeight) / 2);
			}
		}
	}
	
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		if (camera != null) {
			Camera.Parameters params = camera.getParameters();
			params.setPreviewSize(previewSize.width, previewSize.height);
			requestLayout();
			
			camera.setParameters(params);
			camera.startPreview();
		}
	}

	public void surfaceCreated(SurfaceHolder holder) {
		try {
			if (camera != null) {
				camera.setPreviewDisplay(holder);
			}
		} catch (IOException e) {
			Log.e("CameraPreview", "Error setting camera preview: " + e.getMessage());
		}
	}

	public void surfaceDestroyed(SurfaceHolder arg0) {
		try {
			camera.stopPreview();
		} catch (Exception e) {
			Log.e("CameraPreview", "Error stopggin camera preview: " + e.getMessage());
		}
	}

}
