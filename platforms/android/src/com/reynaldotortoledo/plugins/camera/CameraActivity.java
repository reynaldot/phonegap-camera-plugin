package com.reynaldotortoledo.plugins.camera;

import static android.hardware.Camera.Parameters.FOCUS_MODE_AUTO;
import static android.hardware.Camera.Parameters.FOCUS_MODE_MACRO;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import org.apache.cordova.CordovaActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PictureCallback;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;

public class CameraActivity extends CordovaActivity {
	
	public static String ERROR_MESSAGE = "ErrorMessage";
	public static String FILENAME = "Filename";
	public static String IMAGE_URI = "ImageUri";
	public static String QUALITY = "Quality";
	public static String ORIENTATION = "Orientation";
	public static String CORRECT_ORIENTATION = "CorrectOrientation";
	
	private RelativeLayout layout;
	private FrameLayout cameraPreviewView;
	private CameraPreview cameraPreview;
	private ImageButton captureButton;
	private Camera camera;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		
		layout = new RelativeLayout(this);
		layout.setLayoutParams(layoutParams);
		
		createCameraPreview();
		createCaptureButton();
		setContentView(layout);
	}
	
	private void createCameraPreview() {
		cameraPreviewView = new FrameLayout(this);
		FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		
		cameraPreviewView.setLayoutParams(layoutParams);
		layout.addView(cameraPreviewView);
		
		SurfaceView surfaceView = new SurfaceView(this);
		surfaceView.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		
		cameraPreview = new CameraPreview(this, surfaceView);

		cameraPreviewView.addView(cameraPreview);
	}
	
	private void createCaptureButton() {
		captureButton = new ImageButton(getApplicationContext());
		setBitmap(captureButton, "capture_button.png");
		captureButton.setBackgroundColor(Color.TRANSPARENT);
		captureButton.setScaleType(ScaleType.FIT_CENTER);
		RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		layoutParams.bottomMargin = dpToPixels(0);
		layoutParams.leftMargin = dpToPixels(0);
		captureButton.setLayoutParams(layoutParams);
		captureButton.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				setCaptureButtonImageForEvent(event);
				return false;
			}
		});
		captureButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				takePictureWithAutoFocus();
			}
		});
		layout.addView(captureButton);
	}
	
	private void setBitmap(ImageView imageView, String imageName) {
		try {
			InputStream imageStream = getAssets().open("www/img/camera/" + imageName);
			Bitmap bitmap = BitmapFactory.decodeStream(imageStream);
			imageView.setImageBitmap(bitmap);
			imageStream.close();
		} catch (Exception e) {
			Log.e("CameraActivity", "Could not load image", e);
		}
	}
	
	private void setCaptureButtonImageForEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			setBitmap(captureButton, "capture_button_pressed.png");
		} else if (event.getAction() == MotionEvent.ACTION_UP) {
			setBitmap(captureButton, "capture_button.png");
		}
	}
	
	private int dpToPixels(int dp) {
		float density = getResources().getDisplayMetrics().density;
		return Math.round(dp * density);
	}
	
	private void takePictureWithAutoFocus() {
		String focusMode = camera.getParameters().getFocusMode();
		if (focusMode == FOCUS_MODE_AUTO || focusMode == FOCUS_MODE_MACRO) {
			camera.autoFocus(new AutoFocusCallback() {
				@Override
				public void onAutoFocus(boolean arg0, Camera arg1) {
					takePicture();
				}
			});
		} else {
			takePicture();
		}
	}
	
	private void takePicture() {
		try {
			camera.takePicture(null, null, pictureCallback);
		} catch (Exception e) {
			finishWithError("Error capturing image");
		}
	}
	
	private final PictureCallback pictureCallback = new PictureCallback() {
		@Override
		public void onPictureTaken(byte[] jpegData, Camera camera) {
			try {
				String filename = getIntent().getStringExtra(FILENAME);
				int quality = getIntent().getIntExtra(QUALITY, 80);
				File capturedImageFile = new File(getCacheDir(), filename);
				Bitmap bitmap = BitmapFactory.decodeByteArray(jpegData, 0, jpegData.length);
				int rotate = getIntent().getIntExtra(CORRECT_ORIENTATION, 0);
				Matrix matrix = new Matrix();
				matrix.setRotate(rotate);
				bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
				bitmap.compress(CompressFormat.JPEG, quality, new FileOutputStream(capturedImageFile));
				Intent data = new Intent();
				data.putExtra(IMAGE_URI, Uri.fromFile(capturedImageFile).toString());
				setResult(RESULT_OK, data);
				finish();
			} catch (Exception e) {
				finishWithError("Failed To save image");
			}
		}
	};
	
	@Override
	protected void onStart() {
		super.onStart();
		openCamera();
		displayCameraPreview();
	}
	
	private void openCamera() {
		// TODO: 
		// - Handle orientation changes
		// - Support portrait mode 
		// - Correct orientation of resulting picture
		int degrees = getIntent().getIntExtra(ORIENTATION, 0);
		camera = openFrontFacingCamera();
		camera.setDisplayOrientation(degrees);
	}
	
	private void displayCameraPreview() {
		cameraPreview.setCamera(camera);
	}
	
	// TODO: Should be able to select camera via options:
	private Camera openFrontFacingCamera() {
		int cameraCount = 0;
		Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
		cameraCount = Camera.getNumberOfCameras();
		for  (int cameraId = 0; cameraId < cameraCount; cameraId++) {
			Camera.getCameraInfo(cameraId, cameraInfo);
			if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
				try {
					camera = Camera.open(cameraId);
				} catch (RuntimeException e) {
					finishWithError("Camera faild to open");
				}
			}
		}
		return camera;
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if (camera != null) {
			camera.stopPreview();
			cameraPreview.setCamera(null);
			camera.release();
			camera = null;
		}
	}
	 
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		finishWithError("Cancelled camera");
	}
	
	private void finishWithError(String message) {
		Intent data = new Intent().putExtra(ERROR_MESSAGE, message);
		setResult(RESULT_CANCELED, data);
		finish();
	}
}
